# README #

### What is this repository for? ###

慢病论文相关研究用到的程序，是一个文本分类的任务，

### what are the folds for ###

data: 存放原始、中间、结果数据的目录

model：存放模型的目录

python_code：基于python的处理程序（病历文书提取、模型训练、预测）

sif_code：存放sif embedding代码的程序（无监督获取句子向量的第三方程序，只用不改）

sql_version：存放通过数据库进行数据处理的sql文件

### The function of python footages ###

部分代码有重叠

python_code目录下：
code（mohammad写的基于翻译后的英文语料训练模型代码）
model_complain_history_current_medical.ipynb 模型训练方法

en_code（Peter开发的基于翻译成英文语料进行训练、预测的方法）
character_split.py 预处理程序，去除停用词
feature_processing_and_training.py 特征工程与模型训练方法
Medical_record_processing.py 提取xml存储的病历文书标签内容程序（主诉、现病史等）
sif_embedding.py 测试sif embedding的程序

character_embedding_training.py 训练字向量程序
character_split.py 预处理程序，分字（词）+去除停用词
count_comma.py 统计逗号的程序（数据验证）
current_history_check_and_improve.py 现病史分词（数据处理）
feature_processing_and_training.py 特征工程与模型训练方法
Medical_record_processing.py 提取xml存储的病历文书标签内容程序（主诉、现病史等）
research_improve_model_except_complaint_20200426.py 尝试去掉主诉训练的程序（优化）
research_initial_model_use_text_complaint_cur_pre_history_20200423.py 尝试只用主诉、现病史训练（优化）
sif_embedding.py 测试sif embedding的程序
test_vector.py 测试词向量效果
text_translate.py


sql_version目录下：（实验步骤从sql可看出来）
create_dim_and_translate_text_columns.txt 创建维表
create_table.txt 建表
data_explore.txt 初期数据探索与统计
export_data.txt 数据导出
feature_programming.txt 部分特征工程
labeling.txt 按照mohammad提供思路标注
load_data.txt 数据导入
main_diag_distribution_cnt.txt 诊断分布统计
missing_value_statistic.txt 缺失值统计



### How do I get set up? ###
训练词、字向量：python character_embedding_training.py
处理病历xml：python Medical_record_processing.py
模型训练与评价：python feature_processing_and_training.py




