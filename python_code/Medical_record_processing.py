#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
from lxml import etree
from collections import Counter
#medical_records_data='../data/clean_history_data_20200303.csv'
#medical_records_data_raw='../data/patient_info_2019_20200326.csv'
medical_records_data_raw='../data/wenshu_0407.csv'
medical_records_data_raw_excel='../data/test_medical_record.xlsx'
#medical_records_data_dealed='../data/clean_history_data_20200326_dealed_pt2.csv'
#medical_records_data_dealed=['../data/clean_history_data_20200326_dealed_pt1.csv','../data/clean_history_data_20200326_dealed_pt2.csv']
medical_records_data_dealed=['../data/clean_history_data_20200407_dealed.csv']

medical_records_data_mixed1='../data/clean_history_data_20200407_mixed.csv'
medical_records_data_mixed2='../data/clean_history_data_20200326_mixed_pt2.csv'
#medical_records_excel_test_df=pd.read_excel(medical_records_data_raw_excel)
#medical_records_df=pd.read_csv(medical_records_data_raw)

#medical_records_df.columns=['Hosp_District_Code', 'Visit_No', 'EMPI_ID', 'OutHosp_Index_No', 'OutHosp_Index_No.1', 'Date_Birth', 'Sex_Code', 
                #'Visit_Dept_Code', 'Visit_Dept_Name', 'Visit_Dr_Code', 'In_Visit_Room_Date', 'Out_Visit_Room_Date', 'Main_Diag_Code', 
                #'Main_Diag_Name', 'Regist_Categ_Code', 'Regist_Categ_Name', 'Xml_Content_Nohtml']
#medical_records_dealed_df=pd.read_csv(medical_records_data_dealed)


#显示所有列
#pd.set_option('display.max_columns', None)
#显示所有行
#pd.set_option('display.max_rows', None)

#返回每行缺失值的总数
#pharmacy_df.isnull().sum(axis=1)
#返回每列缺失值的总数
#pharmacy_df.isnull().sum(axis=0)


#预处理步骤，删除异常行，不处理时不打开
#isna_list = medical_records_df['Xml_Content_Nohtml'].isna()
#na_index = [index for index in isna_list.index if isna_list[index]]
#medical_records_df=medical_records_df.drop(na_index)
#medical_records_df.reset_index(drop=True, inplace=True)
#text_index=[index for index in medical_records_df['Xml_Content_Nohtml'].index]
#drop_index=[index for index in text_index if not medical_records_df['Xml_Content_Nohtml'].iloc[index].lower().strip().startswith('<root>')]
#medical_records_df=medical_records_df.drop(drop_index)
#drop_index =[]
#for index in medical_records_df['Xml_Content_Nohtml'].index:
    #count_dict=Counter(medical_records_df['Xml_Content_Nohtml'].iloc[index])
    #if count_dict['<'] != count_dict['>']:
        #drop_index.append(index)
#medical_records_df=medical_records_df.drop(drop_index)
#medical_records_df.to_csv('../data/clean_history_data_20200326_dealed.csv',index=None)

#abnormal=[]

#找出异常数据
#for index in medical_records_excel_test_df.index:
    #print(index) 
    ##print(Counter(medical_records_df['Xml_Content_Nohtml'].iloc[index]))
    ##if Counter(medical_records_df['Xml_Content_Nohtml'].iloc[index])['<'] != Counter(medical_records_df['Xml_Content_Nohtml'].iloc[index])['>']:
    ##abnormal.append(index)
    #print(etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//主诉/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//现病史/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//既往史/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//体格检查/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊断/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊疗计划/text()")+
        #etree.XML(medical_records_excel_test_df['Xml_Content_Nohtml'].iloc[index]).xpath("//治疗意见/text()"))


#medical_records_dealed_df=pd.read_csv('../data/clean_history_data_20200407_dealed.csv')

#for index in medical_records_dealed_df['Xml_Content_Nohtml'].index:
    #print(index) 
    ##print(Counter(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]))
    ##if Counter(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index])['<'] != Counter(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index])['>']:
    ##abnormal.append(index)
    #print(etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//主诉/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//现病史/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//既往史/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//体格检查/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊断/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊疗计划/text()")+
        #etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//治疗意见/text()"))


medical_records_data_dealed1='../data/clean_history_data_20200407_dealed.csv'
medical_records_data_dealed2='../data/clean_history_data_20200326_dealed_pt2.csv'


for file_name in medical_records_data_dealed:
    medical_records_dealed_df=pd.read_csv(file_name)
    #提取标签中的文本
    #主诉
    text_complaint_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//主诉/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//默认段落/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//内容/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//病情补充记录/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #现病史
    text_chistory_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//现病史/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #既往史
    text_phistory_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//既往史/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #过敏史
    text_allergystory_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//过敏史/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #家族史
    text_familystory_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//家族史/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #体格检查
    text_examination_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//体格检查/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #诊断
    text_diagnosis_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊断/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊断DIAGNOSIS/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//修正补充诊断/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #诊疗计划
    text_diagnosis_plan_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊疗计划/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//诊疗计划旧/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    
    
    #治疗意见
    text_treatment_opinion_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//治疗意见/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//治疗意见治疗意见TREATMENT/text()")
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//注意事项/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    #医师签名
    text_sign_df=pd.DataFrame([list(medical_records_dealed_df[['Index']].iloc[index].values)
                +etree.XML(medical_records_dealed_df['Xml_Content_Nohtml'].iloc[index]).xpath("//医师签名/text()")
                for index in medical_records_dealed_df['Xml_Content_Nohtml'].index])
    
    text_complaint_df.columns = ['Index','complaint']
    text_chistory_df.columns = ['Index','current_medical_history']
    text_phistory_df.columns = ['Index','previous_history']
    text_allergystory_df.columns = ['Index','allergy_history']
    text_familystory_df.columns = ['Index','family_history']
    text_examination_df.columns = ['Index','physical_examination']
    text_diagnosis_df.columns = ['Index','diagnosis']
    text_diagnosis_plan_df.columns = ['Index','diagnosis_plan']
    text_treatment_opinion_df.columns = ['Index','treatment_opinion']
    text_sign_df.columns = ['Index','doctor_signature']
    
    
    mixed_df = pd.merge(medical_records_dealed_df, text_complaint_df,how='outer',on='Index').drop(['Xml_Content_Nohtml'],axis=1)
    mixed_df = pd.merge(mixed_df, text_chistory_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_phistory_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_allergystory_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_familystory_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_examination_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_diagnosis_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_diagnosis_plan_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_treatment_opinion_df,how='outer',on='Index')
    mixed_df = pd.merge(mixed_df, text_sign_df,how='outer',on='Index')
    
    if file_name==medical_records_data_dealed1:
        mixed_df.to_csv(medical_records_data_mixed1,index=None)
    else:
        mixed_df.to_csv(medical_records_data_mixed2,index=None)


print()