#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Prepare the medical_character_vocab_freq.txt data, will use in sif_embedding.py
'''
from collections import Counter
textfile='../data/all.txt'
stop_word_file='../data/stop_words.txt'

stopwords = open(stop_word_file,'r',encoding='utf-8')
swline = stopwords.readlines()

stopwords_list = [line.strip() for line in swline]

f = open(textfile,'r',encoding='utf-8')
lines = f.readlines()
res = []
for line in lines:
    res+=[character.strip() if character not in stopwords_list else 'unknow' for character in line]

cnt_dict = Counter(res)



print()