#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../sif/src')
import pandas as pd
import jieba
from sklearn.linear_model import LogisticRegression
import data_io,params,SIF_embedding
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score

from sklearn.metrics import roc_auc_score

training_file_path='../data/research_initial_model/training_1293.xlsx'
testing_file_path='../data/research_initial_model/testing_1293.xlsx'

training_file_seg_path='../data/research_initial_model/training_1293_seg.xlsx'
testing_file_seg_path='../data/research_initial_model/test_1293_seg.xlsx'

training_pd = pd.read_excel(training_file_path)
testing_pd = pd.read_excel(testing_file_path)

with open('../data/stop_words.txt', 'r',encoding='utf-8') as f:
    stop_words = f.read().split('\n')


'''
只用文本特征，文本特征用主诉、现病史、既往史：

'''
##显示所有列
##pd.set_option('display.max_columns', None)
##显示所有行
##pd.set_option('display.max_rows', None)
##设置value的显示长度为100，默认为50

##预处理，分词,去停用词等操作

#medical_records_training=[]
#medical_records_testing=[]

##分词
##for sentence in training_pd['medical_records']:
    ##medical_records_training.append(' '.join([word for word in jieba.cut(sentence) if word not in stop_words]))
##for sentence in testing_pd['medical_records']:
    ##medical_records_testing.append(' '.join([word for word in jieba.cut(sentence) if word not in stop_words]))  


##分字
#for sentence in training_pd['medical_records']:
    #medical_records_training.append(' '.join([character for character in sentence.strip() if character not in stop_words]))
#for sentence in testing_pd['medical_records']:
    #medical_records_testing.append(' '.join([character for character in sentence.strip() if character not in stop_words]))  
    

#medical_records_training_pd=pd.Series(medical_records_training)
#medical_records_testing_pd=pd.Series(medical_records_testing)

#medical_records_training_pd.to_csv(training_file_seg_path,index=None,encoding='utf-8')
#medical_records_testing_pd.to_csv(testing_file_seg_path,index=None,encoding='utf-8')



train_x='../data/research_initial_model/training_1293_trainable_x.xlsx'
train_y='../data/research_initial_model/training_1293_trainable_y.xlsx'

test_x='../data/research_initial_model/testing_1293_trainable_x.xlsx'
test_y='../data/research_initial_model/testing_1293_trainable_y.xlsx'


train_x_word='../data/research_initial_model/training_1293_trainable_x_word.xlsx'
test_x_word='../data/research_initial_model/testing_1293_trainable_x_word.xlsx'


train_x_df=pd.read_excel(train_x)
train_y_df=pd.read_excel(train_y)
test_x_df=pd.read_excel(test_x)
test_y_df=pd.read_excel(test_y)


train_x_word_df=pd.read_excel(train_x_word)
test_x_word_df=pd.read_excel(test_x_word)

##训练，使用tfidf特征
#tv = TfidfVectorizer(min_df=5,  max_features=None, 
            #strip_accents='unicode', analyzer='word',#token_pattern=r'\w{1,}',
            #ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1,
            #stop_words = stop_words)

#tv_train = tv.fit_transform(train_x_df.medical_records.values).toarray()
#tv_test = tv.transform(test_x_df.medical_records.values).toarray()

##如果转换为只用文本特征，注释下面6行代码
#tv_train_df = pd.DataFrame(tv_train)

#tv_test_df = pd.DataFrame(tv_test)

#tv_train=pd.concat( [train_x_df,tv_train_df], axis=1 ).fillna(999)
#tv_train.drop(['medical_records'],axis=1,inplace=True)

#tv_test=pd.concat( [test_x_df,tv_test_df], axis=1 ).fillna(999)
#tv_test.drop(['medical_records'],axis=1,inplace=True)



#clf = LogisticRegression(C=1.0, class_weight='balanced', penalty='l2')

#clf.fit(tv_train, train_y_df.if_athma.values)
#y_predict_train = clf.predict_proba(tv_train)[:,1]
#y_predict_test = clf.predict_proba(tv_test)[:,1]

#print ("ROC AUC: train: %0.3f, test: %0.3f " % (
    #roc_auc_score(train_y_df.if_athma.values, y_predict_train), roc_auc_score(test_y_df.if_athma.values, y_predict_test)))

#print()


#训练，使用sif embedding特征（字向量）
#sif embedding
#wordfile = '../sif/data/medical_vec_character.txt' # word vector file, can be downloaded from GloVe website
#wordfile = '../sif/data/medical_token_vec_100.bin' # word vector file, can be downloaded from GloVe website
wordfile = '../sif/data/medical_word_vec_100.bin' # word vector file, can be downloaded from GloVe website

weightfile = '../sif/auxiliary_data/medical_character_vocab_freq.txt' # each line is a word and its frequency
weightpara = 1e-3 # the parameter in the SIF weighting scheme, usually in the range [3e-5, 3e-3]
rmpc = 1 # number of principal components to remove in SIF weighting scheme
(words, We) = data_io.getWordmap(wordfile)
# load word weights
word2weight = data_io.getWordWeight(weightfile, weightpara) # word2weight['str'] is the weight for the word 'str'
weight4ind = data_io.getWeight(words, word2weight) # weight4ind[i] is the weight for the i-th word

##如果是用词向量，放开下面4行代码
# load sentences for medical_record(词向量)
#x_mr_train, m_mr_train= data_io.sentences2idx(train_x_word_df.medical_records.values, words) # x is the array of word indices, m is the binary mask indicating whether there is a word in that location
#w_mr_train = data_io.seq2weight(x_mr_train, m_mr_train, weight4ind) # get word weights


#x_mr_test, m_mr_test= data_io.sentences2idx(test_x_word_df.medical_records.values, words) 
#w_mr_test = data_io.seq2weight(x_mr_test, m_mr_test, weight4ind) 

##如果是用字向量，放开下面4行代码
# load sentences for medical_record(字向量)
x_mr_train, m_mr_train= data_io.sentences2idx(train_x_df.medical_records.values, words) # x is the array of word indices, m is the binary mask indicating whether there is a word in that location
w_mr_train = data_io.seq2weight(x_mr_train, m_mr_train, weight4ind) # get word weights


x_mr_test, m_mr_test= data_io.sentences2idx(test_x_df.medical_records.values, words) 
w_mr_test = data_io.seq2weight(x_mr_test, m_mr_test, weight4ind) 


# set parameters
params = params.params()
params.rmpc = rmpc
# get SIF embedding for medical_record
embedding_mr_train = SIF_embedding.SIF_embedding(We, x_mr_train, w_mr_train, params) # embedding[i,:] is the embedding for sentence i
embedding_mr_test = SIF_embedding.SIF_embedding(We, x_mr_test, w_mr_test, params) # embedding[i,:] is the embedding for sentence i


##如果转换为只用文本特征，注释下面8行代码
embedding_mr_train_df = pd.DataFrame(embedding_mr_train)
embedding_mr_train_df.columns=['medical_record_emb_1','medical_record_emb_2','medical_record_emb_3','medical_record_emb_4','medical_record_emb_5','medical_record_emb_6','medical_record_emb_7','medical_record_emb_8','medical_record_emb_9','medical_record_emb_10','medical_record_emb_11','medical_record_emb_12','medical_record_emb_13','medical_record_emb_14','medical_record_emb_15','medical_record_emb_16','medical_record_emb_17','medical_record_emb_18','medical_record_emb_19','medical_record_emb_20','medical_record_emb_21','medical_record_emb_22','medical_record_emb_23','medical_record_emb_24','medical_record_emb_25','medical_record_emb_26','medical_record_emb_27','medical_record_emb_28','medical_record_emb_29','medical_record_emb_30','medical_record_emb_31','medical_record_emb_32','medical_record_emb_33','medical_record_emb_34','medical_record_emb_35','medical_record_emb_36','medical_record_emb_37','medical_record_emb_38','medical_record_emb_39','medical_record_emb_40','medical_record_emb_41','medical_record_emb_42','medical_record_emb_43','medical_record_emb_44','medical_record_emb_45','medical_record_emb_46','medical_record_emb_47','medical_record_emb_48','medical_record_emb_49','medical_record_emb_50','medical_record_emb_51','medical_record_emb_52','medical_record_emb_53','medical_record_emb_54','medical_record_emb_55','medical_record_emb_56','medical_record_emb_57','medical_record_emb_58','medical_record_emb_59','medical_record_emb_60','medical_record_emb_61','medical_record_emb_62','medical_record_emb_63','medical_record_emb_64','medical_record_emb_65','medical_record_emb_66','medical_record_emb_67','medical_record_emb_68','medical_record_emb_69','medical_record_emb_70','medical_record_emb_71','medical_record_emb_72','medical_record_emb_73','medical_record_emb_74','medical_record_emb_75','medical_record_emb_76','medical_record_emb_77','medical_record_emb_78','medical_record_emb_79','medical_record_emb_80','medical_record_emb_81','medical_record_emb_82','medical_record_emb_83','medical_record_emb_84','medical_record_emb_85','medical_record_emb_86','medical_record_emb_87','medical_record_emb_88','medical_record_emb_89','medical_record_emb_90','medical_record_emb_91','medical_record_emb_92','medical_record_emb_93','medical_record_emb_94','medical_record_emb_95','medical_record_emb_96','medical_record_emb_97','medical_record_emb_98','medical_record_emb_99','medical_record_emb_100']

embedding_mr_test_df = pd.DataFrame(embedding_mr_test)
embedding_mr_test_df.columns=['medical_record_emb_1','medical_record_emb_2','medical_record_emb_3','medical_record_emb_4','medical_record_emb_5','medical_record_emb_6','medical_record_emb_7','medical_record_emb_8','medical_record_emb_9','medical_record_emb_10','medical_record_emb_11','medical_record_emb_12','medical_record_emb_13','medical_record_emb_14','medical_record_emb_15','medical_record_emb_16','medical_record_emb_17','medical_record_emb_18','medical_record_emb_19','medical_record_emb_20','medical_record_emb_21','medical_record_emb_22','medical_record_emb_23','medical_record_emb_24','medical_record_emb_25','medical_record_emb_26','medical_record_emb_27','medical_record_emb_28','medical_record_emb_29','medical_record_emb_30','medical_record_emb_31','medical_record_emb_32','medical_record_emb_33','medical_record_emb_34','medical_record_emb_35','medical_record_emb_36','medical_record_emb_37','medical_record_emb_38','medical_record_emb_39','medical_record_emb_40','medical_record_emb_41','medical_record_emb_42','medical_record_emb_43','medical_record_emb_44','medical_record_emb_45','medical_record_emb_46','medical_record_emb_47','medical_record_emb_48','medical_record_emb_49','medical_record_emb_50','medical_record_emb_51','medical_record_emb_52','medical_record_emb_53','medical_record_emb_54','medical_record_emb_55','medical_record_emb_56','medical_record_emb_57','medical_record_emb_58','medical_record_emb_59','medical_record_emb_60','medical_record_emb_61','medical_record_emb_62','medical_record_emb_63','medical_record_emb_64','medical_record_emb_65','medical_record_emb_66','medical_record_emb_67','medical_record_emb_68','medical_record_emb_69','medical_record_emb_70','medical_record_emb_71','medical_record_emb_72','medical_record_emb_73','medical_record_emb_74','medical_record_emb_75','medical_record_emb_76','medical_record_emb_77','medical_record_emb_78','medical_record_emb_79','medical_record_emb_80','medical_record_emb_81','medical_record_emb_82','medical_record_emb_83','medical_record_emb_84','medical_record_emb_85','medical_record_emb_86','medical_record_emb_87','medical_record_emb_88','medical_record_emb_89','medical_record_emb_90','medical_record_emb_91','medical_record_emb_92','medical_record_emb_93','medical_record_emb_94','medical_record_emb_95','medical_record_emb_96','medical_record_emb_97','medical_record_emb_98','medical_record_emb_99','medical_record_emb_100']


embedding_mr_train=pd.concat( [train_x_df,embedding_mr_train_df], axis=1 ).fillna(999)
embedding_mr_train.drop(['medical_records'],axis=1,inplace=True)

embedding_mr_test=pd.concat( [test_x_df,embedding_mr_test_df], axis=1 ).fillna(999)
embedding_mr_test.drop(['medical_records'],axis=1,inplace=True)

clf = LogisticRegression(C=1.0, class_weight='balanced', penalty='l2')

clf.fit(embedding_mr_train, train_y_df.if_athma.values)
y_predict_train = clf.predict_proba(embedding_mr_train)[:,1]
y_predict_test = clf.predict_proba(embedding_mr_test)[:,1]

print ("ROC AUC: train: %0.3f, test: %0.3f " % (
    roc_auc_score(train_y_df.if_athma.values, y_predict_train), roc_auc_score(test_y_df.if_athma.values, y_predict_test)))



print()