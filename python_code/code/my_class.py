import re
from sklearn.feature_extraction.text import TfidfVectorizer

import nltk
from nltk.tokenize import word_tokenize
from gensim.utils import lemmatize

# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')

class PreProcess():
    def __init__(self):
        pass
    
    def clean_text(self, text, stop_words_):
        '''Retrun a clean text'''
        
        text = text.lower()
        '''Remove emails, web address''' 
        text = re.sub("\S*@\S*\s?", "", text)
        text = re.sub("^rt ", "", text)
        text = re.sub("(https?\://)\S+", "", text)
        text = re.sub("(www?\://)\S+", "", text)
        text = re.sub("(www?\.)\S+", "", text)

        '''Remove new line characters'''
        text = re.sub('\s+', ' ', text)

        '''Remove distracting single quotes'''
        text = re.sub("\'", "", text)

        '''Remove punctuations'''
        text = re.sub("[^a-zA-Z\\s]", " ", text)

        '''remove words with less than two unique characters'''
        text_ = ' '.join([word for word in word_tokenize(text) if 
             (word not in stop_words_) & (len(''.join(set(word)))>2)])
        return text


    def lemmatize_text(self, text_):
        
        lemmat_text = ' '.join(
            [
                wd.decode('utf-8').split('/')[0] for wd in lemmatize(text_) 
                if len(''.join(set(wd.decode('utf-8').split('/')[0])))>2
            ]
        )

        return lemmat_text

    def tfidf(self, X,stop_words=None):    

        tfidf_vectorizer = TfidfVectorizer(max_df=1.0,stop_words=stop_words)
        tfidf_vectorizer.fit(X)
        self.tfidf_vectorizer = tfidf_vectorizer        
        return self
    def __init__(self, fix_catg=1000):
                
        self.fix_catg = fix_catg
        
    def __estimator(self):
        
        estimator_ = RandomForestClassifier(
            n_estimators=20,
            max_depth=None,
            max_features=1.0, 
            random_state=42)
            
        return estimator_

    def build_estimators(self, X_train, X_test, y_train, y_test):
                
        categories = self.categories        
        estimators_ = dict.fromkeys(categories)
        
        for category in categories: 
            
            print('building classifier for category:{}'.format(category))
            
            estimator_ = self.__estimator()            
            y_temp_train = y_train.copy()
            y_temp_train[y_temp_train!=category] = self.fix_catg
          
            y_temp_test = y_test.copy()
            y_temp_test[y_temp_test!=category] = self.fix_catg
            
            '''
            In category_ordered, lables are stored so that 
            the second element is the minor category in training data. 
            This is used in calculating f1_score
            '''
            category_ordered = y_temp_train.value_counts().sort_values(
                ascending=False
            ).index.tolist()
            
            dict_category = {0: category_ordered[0], 1: category_ordered[1]}
            
            estimator_.fit(X_train, y_temp_train)
        
            # finding the index of the minor category
            idx_minor_category = np.where(estimator_.classes_==category_ordered[1])[0][0]
            if hasattr(self, 'thr_tuned'):
                thr = self.thr_sel[category]
            else:
                thr = 0.5
            print(
                'f1 for train data: ', self.calc_perform(
                    y_temp_train, estimator_.predict(X_train),
                        pos_label=category_ordered[1]
                )
            )
            print(
                'f1 for test data before fine tuning threshold: {}, count categories in test: {}'.format(
                    self.calc_perform(
                        y_temp_test, estimator_.predict(X_test),
                        pos_label=category_ordered[1]
                    ), Counter(y_temp_test.values)
                )
            )
            
            print('-'*70)
            estimators_[category] = {
                'estimator_': estimator_,
                'thr': thr,
                'category_ordered': category_ordered,
                'dict_category': dict_category,
                'idx_minor_category': idx_minor_category 
            }
                                                   
        print('*'*70)
        return estimators_         
    
    def __fine_tune_thresholds(self, estimators_, X, y):

        # candicates for threshold fine-tuning in clf_base        
        thr_cands = np.arange(0.0, 1.0, 0.1)
        thr_tuned = dict.fromkeys(estimators_.keys())

        for category in estimators_.keys(): 
            print('fine tuning threshold for category:{}'.format(category))
            model_info = copy.deepcopy(estimators_[category])
            
            estimator_ = model_info['estimator_']
            category_ordered = model_info['category_ordered']
            idx_minor_category = model_info['idx_minor_category']
            
            y_temp = y.copy()
            y_temp[y_temp!=category] = self.fix_catg
            y_proba = estimator_.predict_proba(X)[:,idx_minor_category]

            '''
            This dic is used for prediction. 
            It maps 0s and 1s after probability calibration to
            their corresponding categories.
            ''' 
            dict_category = model_info['dict_category'] 

            f1_temp_lst = []

            for thr in thr_cands:
                f1_temp_lst.append(
                    self.calc_perform(
                        y_temp,
                        list(map(dict_category.get, 
                                 (y_proba>thr).astype(int))),
                        pos_label=category_ordered[1]
                    )
                )
            f1_max = max(f1_temp_lst)
            thr_sel = round(thr_cands[f1_temp_lst.index(f1_max)],2)
            thr_tuned[category] =thr_sel
            print(
                'For category {}, best threshold={} with f1 score={}'.format(
                    category, thr_sel, f1_max))       
            print('-'*70)
        return thr_tuned
            
            
    def __predict_base(self, estimator_, X):   
        
        y_proba = estimator_['estimator_'].predict_proba(X)[:,estimator_['idx_minor_category']]
        y_pred = list(map(estimator_['dict_category'].get, (y_proba>estimator_['thr']).astype(int)))
        
        return y_pred, y_proba
    
    def predict(self, X):
        
        estimators_ = self.estimators_        
        df_pred = pd.DataFrame([], columns=estimators_.keys(), index=range(X.shape[0]))
        
        for category in estimators_:
            y_pred,_ = self.__predict_base(estimators_[category], X)
            df_pred.loc[:, category] = y_pred
        
        return df_pred.max(axis=1), df_pred
    
    def predict_proba(self, X):

        estimators_ = self.estimators_        
        df_pred_proba = pd.DataFrame([], columns=estimators_.keys(), index=range(X.shape[0]))
        for category in estimators_:
            _, y_proba = self.__predict_base(estimators_[category], X)
            df_pred_proba.loc[:, category] = y_proba

        return df_pred_proba
    
    def calc_perform(self, y_true, y_pred, pos_label=[]):
        
        if pos_label: 
        
            f1 = f1_score(
                y_true, y_pred, 
                pos_label=pos_label)
        else:
            f1 = f1_score(
                y_true, y_pred,                  
                average='weighted'
            )
            
        return f1
            

    def fit(self, X_train, X_test, y_train, y_test):     
        
        categories = y_train.sort_values().unique()
        self.categories = categories
        estimators_ = self.build_estimators(X_train, X_test, y_train, y_test)
        thr_tuned = self.__fine_tune_thresholds(
                estimators_, X_test, y_test
        )
        self.thr_tuned = thr_tuned
        for category in estimators_:
            estimators_[category]['thr'] = thr_tuned[category]
        
        self.estimators_ = estimators_
        print('Classifiers are built!')
        print('*'*70)
