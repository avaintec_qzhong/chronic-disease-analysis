#!/usr/bin/env python
# -*- coding: utf-8 -*-
from collections import Counter
import jieba
import pkuseg

textfile='../data/all.txt'
textfile_aftersplit='../data/all_split.txt'
stop_word_file='../data/stop_words.txt'

seg = pkuseg.pkuseg()
stopwords = open(stop_word_file,'r',encoding='utf-8')
swline = stopwords.readlines()

stopwords_list = [line.strip() for line in swline]

f = open(textfile,'r',encoding='utf-8')
lines = f.readlines()
res = []
#字符级分割
for line in lines:
    res.append(' '.join([character if character not in stopwords_list else '<unk>' for character in line.strip()]))

#分词
#for line in lines:
    #res.append(' '.join([character if character not in stopwords_list else '<unk>' for character in seg.cut(line.strip())]))

#cnt_dict = Counter(res)


fo = open(textfile_aftersplit, "w", encoding='utf-8')
for line in res:
    fo.write(line+'\n')
fo.close()
print()