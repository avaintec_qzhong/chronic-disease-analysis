#!/usr/bin/env python
# -*- coding: utf-8 -*-
import jieba
import pandas as pd
file_path='../data/research_text_modifying/current_medical_history_modify.csv'
stop_word_file='../data/stop_words.txt'

stopwords = open(stop_word_file,'r',encoding='utf-8')
swline = stopwords.readlines()

stopwords_list = [line.strip() for line in swline]


ch_df = pd.read_csv(file_path,encoding='utf-8')

res=[]

count_res=[]
ch_split_df=pd.read_csv('../data/research_text_modifying/current_medical_history_modify_cut_no_unk.csv',encoding='utf-8')
ch_split_df.columns=['index','text_cut']
for sentence in ch_split_df['text_cut']:
    for words in sentence.split(' '):
        count_res.append(words)

for line in ch_df['current_medical_history']:
    res.append(' '.join([character for character in jieba.cut(line.strip())]))
pd.Series(res).to_csv('../data/research_text_modifying/current_medical_history_modify_cut_no_unk.csv',encoding='utf-8')
print()





