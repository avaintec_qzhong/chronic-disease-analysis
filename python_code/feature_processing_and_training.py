#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
sys.path.append('../sif/src')
import pandas as pd
import numpy as np
from collections import Counter
import data_io,params,SIF_embedding
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.externals import joblib
from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
from sklearn.ensemble import RandomForestClassifier
import scipy.stats
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc


clf = GridSearch()


training_file_path='../data/research_initial_model/training_1293.xlsx'
testing_file_path='../data/research_initial_model/testing_1293.xlsx'

training_file_seg_path='../data/research_initial_model/training_1293_seg.xlsx'
testing_file_seg_path='../data/research_initial_model/test_1293_seg.xlsx'

training_pd = pd.read_excel(training_file_path)
testing_pd = pd.read_excel(testing_file_path)


#存储或加载模型
def save_model(model,model_path):
    joblib.dump(model, model_path)    
    return 

def load_model(model_path):
    return joblib.load(model_path)


data_csv='../data/outpatient_training_table.csv'
stop_word_file='../data/stop_words.txt'


#data_hi_csv='../data/outpatient_training_table_have_index.csv'
#mr_hi_csv='../data/medical_records_have_index.csv'
#re_hi_csv='../data/exam_result_have_index.csv'


stopwords = open(stop_word_file,'r',encoding='utf-8')
swline = stopwords.readlines()

stopwords_list = [line.strip() for line in swline]


##显示所有列
##pd.set_option('display.max_columns', None)
##显示所有行
##pd.set_option('display.max_rows', None)
##设置value的显示长度为100，默认为50


data_df=pd.read_csv(data_csv)

##将medicare_categ_code和main_ciag_code转为自然数字
#medicare_categ_code=data_df['Medicare_Categ_Code']
##main_ciag_code=data_df['Main_Diag_Code']

#medicare_categ_code_feature={medicare_categ_code_cate:list(medicare_categ_code.drop_duplicates().dropna()).index(medicare_categ_code_cate) for medicare_categ_code_cate in list(medicare_categ_code.drop_duplicates().dropna())}
##main_ciag_code_feature={main_ciag_code_cate:list(main_ciag_code.drop_duplicates().dropna()).index(main_ciag_code_cate) for main_ciag_code_cate in list(main_ciag_code.drop_duplicates().dropna())}
#data_df['Medicare_Categ_Code']=data_df['Medicare_Categ_Code'].map(medicare_categ_code_feature)
##data_df['Main_Diag_Code']=data_df['Main_Diag_Code'].map(main_ciag_code_feature)


##character embedding并补全null为unknow
medical_record = data_df['medical_records']
#exam_result = data_df['EXAM_RESULT']
#exam_result= exam_result.fillna('unknow')

res_medical_record = []
for line in medical_record:
    res_medical_record.append(' '.join([character.strip() for character in line if character not in stopwords_list]))

#res_exam_result = []
#for line in exam_result:
    #if line=='unknow':
        #res_exam_result.append('unknow')
    #else:
        #res_exam_result.append(' '.join([character.strip() for character in line if character not in stopwords_list]))

#sif embedding
wordfile = '../sif/data/medical_vec_character.txt' # word vector file, can be downloaded from GloVe website
weightfile = '../sif/auxiliary_data/medical_character_vocab_freq.txt' # each line is a word and its frequency
weightpara = 1e-3 # the parameter in the SIF weighting scheme, usually in the range [3e-5, 3e-3]
rmpc = 1 # number of principal components to remove in SIF weighting scheme
(words, We) = data_io.getWordmap(wordfile)
# load word weights
word2weight = data_io.getWordWeight(weightfile, weightpara) # word2weight['str'] is the weight for the word 'str'
weight4ind = data_io.getWeight(words, word2weight) # weight4ind[i] is the weight for the i-th word
# load sentences for medical_record
x_mr, m_mr= data_io.sentences2idx(res_medical_record, words) # x is the array of word indices, m is the binary mask indicating whether there is a word in that location
w_mr = data_io.seq2weight(x_mr, m_mr, weight4ind) # get word weights

# set parameters
params = params.params()
params.rmpc = rmpc
# get SIF embedding for medical_record
embedding_mr = SIF_embedding.SIF_embedding(We, x_mr, w_mr, params) # embedding[i,:] is the embedding for sentence i



## load sentences for res_exam_result
#x_re, m_re= data_io.sentences2idx(res_exam_result, words) # x is the array of word indices, m is the binary mask indicating whether there is a word in that location
#w_re = data_io.seq2weight(x_re, m_re, weight4ind) # get word weights

## get SIF embedding for res_exam_result
#embedding_re = SIF_embedding.SIF_embedding(We, x_re, w_re, params) # embedding[i,:] is the embedding for sentence i

#mr_emb_df = pd.DataFrame(embedding_mr)
#mr_emb_df.columns=['medical_records_emb_1','medical_records_emb_2','medical_records_emb_3','medical_records_emb_4','medical_records_emb_5','medical_records_emb_6','medical_records_emb_7','medical_records_emb_8','medical_records_emb_9','medical_records_emb_10','medical_records_emb_11','medical_records_emb_12','medical_records_emb_13','medical_records_emb_14','medical_records_emb_15','medical_records_emb_16','medical_records_emb_17','medical_records_emb_18','medical_records_emb_19','medical_records_emb_20','medical_records_emb_21','medical_records_emb_22','medical_records_emb_23','medical_records_emb_24','medical_records_emb_25','medical_records_emb_26','medical_records_emb_27','medical_records_emb_28','medical_records_emb_29','medical_records_emb_30','medical_records_emb_31','medical_records_emb_32','medical_records_emb_33','medical_records_emb_34','medical_records_emb_35','medical_records_emb_36','medical_records_emb_37','medical_records_emb_38','medical_records_emb_39','medical_records_emb_40','medical_records_emb_41','medical_records_emb_42','medical_records_emb_43','medical_records_emb_44','medical_records_emb_45','medical_records_emb_46','medical_records_emb_47','medical_records_emb_48','medical_records_emb_49','medical_records_emb_50','medical_records_emb_51','medical_records_emb_52','medical_records_emb_53','medical_records_emb_54','medical_records_emb_55','medical_records_emb_56','medical_records_emb_57','medical_records_emb_58','medical_records_emb_59','medical_records_emb_60','medical_records_emb_61','medical_records_emb_62','medical_records_emb_63','medical_records_emb_64','medical_records_emb_65','medical_records_emb_66','medical_records_emb_67','medical_records_emb_68','medical_records_emb_69','medical_records_emb_70','medical_records_emb_71','medical_records_emb_72','medical_records_emb_73','medical_records_emb_74','medical_records_emb_75','medical_records_emb_76','medical_records_emb_77','medical_records_emb_78','medical_records_emb_79','medical_records_emb_80','medical_records_emb_81','medical_records_emb_82','medical_records_emb_83','medical_records_emb_84','medical_records_emb_85','medical_records_emb_86','medical_records_emb_87','medical_records_emb_88','medical_records_emb_89','medical_records_emb_90','medical_records_emb_91','medical_records_emb_92','medical_records_emb_93','medical_records_emb_94','medical_records_emb_95','medical_records_emb_96','medical_records_emb_97','medical_records_emb_98','medical_records_emb_99','medical_records_emb_100']

#re_emb_df = pd.DataFrame(embedding_re)
#re_emb_df.columns=['exam_result_emb_1','exam_result_emb_2','exam_result_emb_3','exam_result_emb_4','exam_result_emb_5','exam_result_emb_6','exam_result_emb_7','exam_result_emb_8','exam_result_emb_9','exam_result_emb_10','exam_result_emb_11','exam_result_emb_12','exam_result_emb_13','exam_result_emb_14','exam_result_emb_15','exam_result_emb_16','exam_result_emb_17','exam_result_emb_18','exam_result_emb_19','exam_result_emb_20','exam_result_emb_21','exam_result_emb_22','exam_result_emb_23','exam_result_emb_24','exam_result_emb_25','exam_result_emb_26','exam_result_emb_27','exam_result_emb_28','exam_result_emb_29','exam_result_emb_30','exam_result_emb_31','exam_result_emb_32','exam_result_emb_33','exam_result_emb_34','exam_result_emb_35','exam_result_emb_36','exam_result_emb_37','exam_result_emb_38','exam_result_emb_39','exam_result_emb_40','exam_result_emb_41','exam_result_emb_42','exam_result_emb_43','exam_result_emb_44','exam_result_emb_45','exam_result_emb_46','exam_result_emb_47','exam_result_emb_48','exam_result_emb_49','exam_result_emb_50','exam_result_emb_51','exam_result_emb_52','exam_result_emb_53','exam_result_emb_54','exam_result_emb_55','exam_result_emb_56','exam_result_emb_57','exam_result_emb_58','exam_result_emb_59','exam_result_emb_60','exam_result_emb_61','exam_result_emb_62','exam_result_emb_63','exam_result_emb_64','exam_result_emb_65','exam_result_emb_66','exam_result_emb_67','exam_result_emb_68','exam_result_emb_69','exam_result_emb_70','exam_result_emb_71','exam_result_emb_72','exam_result_emb_73','exam_result_emb_74','exam_result_emb_75','exam_result_emb_76','exam_result_emb_77','exam_result_emb_78','exam_result_emb_79','exam_result_emb_80','exam_result_emb_81','exam_result_emb_82','exam_result_emb_83','exam_result_emb_84','exam_result_emb_85','exam_result_emb_86','exam_result_emb_87','exam_result_emb_88','exam_result_emb_89','exam_result_emb_90','exam_result_emb_91','exam_result_emb_92','exam_result_emb_93','exam_result_emb_94','exam_result_emb_95','exam_result_emb_96','exam_result_emb_97','exam_result_emb_98','exam_result_emb_99','exam_result_emb_100']



##缺失值填充+导出
#data_df=data_df.drop(['medical_records','EXAM_RESULT'],axis=1)
#data_df=data_df.fillna(999)

#data_df.to_csv(data_hi_csv,encoding='utf-8')
#mr_emb_df.to_csv(mr_hi_csv,encoding='utf-8')
#re_emb_df.to_csv(re_hi_csv,encoding='utf-8')

data_hi_csv='../data/outpatient_training_table_have_index.csv'
mr_hi_csv='../data/medical_records_have_index.csv'
re_hi_csv='../data/exam_result_have_index.csv'

svm_model_path='../model/svm_predict_if_athma.model'
svm_model_ne_path='../model/svm_predict_if_athma_no_examination.model'
data_hi_df=pd.read_csv(data_hi_csv)
mr_hi_df=pd.read_csv(mr_hi_csv)
re_hi_df=pd.read_csv(re_hi_csv)


count_res=[]
count_res_only1=[]
count_res_only1_colname=[]
for col in data_hi_df.columns:
    counter = Counter(data_hi_df[col].values)
    if len(counter)==1:
        count_res_only1.append({col:counter})
        count_res_only1_colname.append(col)
    else:
        count_res.append({col:counter})


data_hi_df.drop(['EMPI_ID', 'OutHosp_Index_No', 'Visit_No'],axis=1,inplace=True)
data_hi_df.drop(count_res_only1_colname,axis=1,inplace=True)   


mixed_df = pd.merge(data_hi_df, mr_hi_df,how='outer',on='index')
#mixed_df = pd.merge(mixed_df, re_hi_df,how='outer',on='index')

mixed_df=mixed_df.sample(frac=1)

y=mixed_df['if_athma']
mixed_df.drop(['index','if_athma'],axis=1,inplace=True)
X=mixed_df

select_model_rf = RandomForestClassifier()
#select_model_rf.fit(X_train, y_train)
#select_model_rf.fit(X, y)
#importances=select_model_rf.feature_importances_
#peason=[scipy.stats.pearsonr(X[x], y)[0] for x in X.columns]
#spearman=[scipy.stats.spearmanr(X[x], y).correlation for x in X.columns]
#kendall=[scipy.stats.kendalltau(X[x], y).correlation for x in X.columns]

##测试集，训练集切分
#respiratory_train_x_csv='../data/training_data/respiratory_train_x.csv'
#respiratory_test_x_csv='../data/training_data/respiratory_test_x.csv'
#respiratory_train_y_csv='../data/training_data/respiratory_train_y.csv'
#respiratory_test_y_csv='../data/training_data/respiratory_test_y.csv'

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30, random_state=0)


#X_train.to_csv(respiratory_train_x_csv,index=0)
#X_test.to_csv(respiratory_test_x_csv,index=0)
#y_train.to_csv(respiratory_train_y_csv,index=0)
#y_test.to_csv(respiratory_test_y_csv,index=0)


#读取数据
#X_train=pd.read_csv(respiratory_train_x_csv)
#X_test=pd.read_csv(respiratory_test_x_csv)
#y_train=pd.read_csv(respiratory_train_y_csv)
#y_test=pd.read_csv(respiratory_test_y_csv)


sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)




def svm_training(model_path,x_train,y_train,if_adujst=False,params=None):
    if if_adujst:
        if __name__=='__main__':
            SVM = SVC(kernel='rbf', probability=True)
            clf = GridSearchCV(SVM,params,scoring = 'neg_log_loss',n_jobs=15,verbose=1)
            clf.fit(x_train, y_train)
            best_estimator = clf.best_estimator_
            print(clf.best_score_)
            print(clf.best_params_)
            save_model(best_estimator, model_path)
    else:  
        clf = SVC(kernel='rbf', probability=True)
        clf.fit(x_train,y_train)
        save_model(clf, model_path)  
        
params_svm = dict(C=[0.001, 0.01, 0.1, 1, 10, 100, 1000],gamma=[0.001, 0.0001])
#svm_training(svm_model_path, X_train_std, y_train,if_adujst=False,params=params_svm)
#svm_training(svm_model_ne_path, X_train_std, y_train,if_adujst=False,params=params_svm)

#SVM = joblib.load(svm_model_path)
SVM = joblib.load(svm_model_ne_path)
svm_model_ne_path
SVM_y_predict_train=SVM.predict(X_train_std)
SVM_y_predict_test=SVM.predict(X_test_std)


y_score = SVM.decision_function(X_test_std)


print('training sets accuracy:')
print(accuracy_score(y_true=y_train, y_pred=SVM_y_predict_train))
print('testing sets accuracy:')
print(accuracy_score(y_true=y_test, y_pred=SVM_y_predict_test))
print('training sets confusion matrix:')
print(classification_report(y_train, SVM_y_predict_train))
print('testing sets confusion matrix:')
print(classification_report(y_test, SVM_y_predict_test))




# Compute ROC curve and ROC area for each class
fpr,tpr,threshold = roc_curve(y_test, y_score) ###计算真正率和假正率
roc_auc = auc(fpr,tpr) ###计算auc的值

plt.figure()
lw = 2
plt.figure(figsize=(10,10))
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f)' % roc_auc) ###假正率为横坐标，真正率为纵坐标做曲线
plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()



print()