#新建维表（exam_code,exam_result,exam_result_en）

drop table if exists dim_complaint;
CREATE TABLE IF NOT EXISTS dim_complaint(
index_id int comment 'ID',
complaint varchar(3000) comment '主诉',
complaint_en varchar(3000) comment '主诉-en'
)DEFAULT CHARSET=utf8;

ALTER TABLE dim_complaint ADD INDEX dim_complaint_index_name (complaint(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/complaint.csv'
INTO TABLE dim_complaint 
FIELDS TERMINATED BY '&' 
IGNORE 1 LINES;



drop table if exists dim_current_medical_history;
CREATE TABLE IF NOT EXISTS dim_current_medical_history(
index_id int comment 'ID',
current_medical_history varchar(3000) comment '现病史',
current_medical_history_en varchar(3000) comment '现病史-en'
)DEFAULT CHARSET=utf8;


ALTER TABLE dim_current_medical_history ADD INDEX dim_current_medical_history_index_name (current_medical_history(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/current_medical_history.csv'
INTO TABLE dim_current_medical_history 
FIELDS TERMINATED BY '@' 
IGNORE 1 LINES;



drop table if exists dim_diagnosis;
CREATE TABLE IF NOT EXISTS dim_diagnosis(
index_id int comment 'ID',
diagnosis varchar(3000) comment '诊断',
diagnosis_en varchar(3000) comment '诊断-en'
)DEFAULT CHARSET=utf8;

ALTER TABLE dim_diagnosis ADD INDEX dim_diagnosis_index_name (diagnosis(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/diagnosis.csv'
INTO TABLE dim_diagnosis 
FIELDS TERMINATED BY '&' 
IGNORE 1 LINES;


drop table if exists dim_exam_result;
CREATE TABLE IF NOT EXISTS dim_exam_result(
index_id int comment 'ID',
EXAM_RESULT varchar(3000) comment '体检结果',
EXAM_RESULT_en varchar(3000) comment '体检结果-en'
)DEFAULT CHARSET=utf8;

ALTER TABLE dim_exam_result ADD INDEX dim_exam_result_index_name (exam_result(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/EXAM_RESULT.csv'
INTO TABLE dim_exam_result 
FIELDS TERMINATED BY '&' 
IGNORE 1 LINES;



drop table if exists dim_physical_examination;
CREATE TABLE IF NOT EXISTS dim_physical_examination(
index_id int comment 'ID',
physical_examination varchar(3000) comment '体格检查',
physical_examination_en varchar(3000) comment '体格检查-en'
)DEFAULT CHARSET=utf8;

ALTER TABLE dim_physical_examination ADD INDEX dim_physical_examination_index_name (physical_examination(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/physical_examination.csv'
INTO TABLE dim_physical_examination 
FIELDS TERMINATED BY '&' 
IGNORE 1 LINES;


drop table if exists dim_previous_history;
CREATE TABLE IF NOT EXISTS dim_previous_history(
index_id int comment 'ID',
previous_history varchar(3000) comment '既往史',
previous_history_en varchar(3000) comment '既往史-en'
)DEFAULT CHARSET=utf8;

ALTER TABLE dim_previous_history ADD INDEX dim_previous_history_index_name (previous_history(1000));

LOAD DATA INFILE 'E:/project_data/dim_translate_tables/index_adding/previous_history.csv'
INTO TABLE dim_previous_history  
FIELDS TERMINATED BY '&'
IGNORE 1 LINES;









