#字段值分布统计：
#门诊
outpatient_user_info
select Sex_Code             ,count(1) from outpatient_user_info group by Sex_Code             ;
select Main_Diag_Name       ,count(1) from outpatient_user_info group by Main_Diag_Name   order by count(1) desc     ;
select Regist_Categ_Code    ,count(1) from outpatient_user_info group by Regist_Categ_Code    ;
select Regist_Categ_Name    ,count(1) from outpatient_user_info group by Regist_Categ_Name    ;
select Visit_Status_Flag    ,count(1) from outpatient_user_info group by Visit_Status_Flag    ;
select Emerg_Flag           ,count(1) from outpatient_user_info group by Emerg_Flag           ;
select TCM_Observe_Result   ,count(1) from outpatient_user_info group by TCM_Observe_Result   ;
select age_distribute,count(1) from 
(select 
case when age<=3 then '<=3' 
when age>3 and age<=6 then '3~6'
when age>6 and age<=9 then '6~9'
when age>9 and age<=12 then '9~12'   
when age>12 and age<=15 then '12~15' 
when age>15 and age<=18 then '15~18' 
when age>18 then '>18' end age_distribute  
from (SELECT year( from_days( datediff( now( ), Date_Birth))) age from outpatient_user_info) tin) t 
group by age_distribute;



select count(distinct OutHosp_Index_No  )OutHosp_Index_No   from outpatient_user_info;
select count(distinct Sex_Code          )Sex_Code           from outpatient_user_info;
select count(distinct Visit_Dept_Name   )Visit_Dept_Name    from outpatient_user_info;
select count(distinct Visit_Date        )Visit_Date         from outpatient_user_info;
select count(distinct Main_Diag_Code    )Main_Diag_Code     from outpatient_user_info;
select count(distinct Main_Diag_Name    )Main_Diag_Name     from outpatient_user_info;
select count(distinct Regist_Categ_Code )Regist_Categ_Code  from outpatient_user_info;
select count(distinct Regist_Categ_Name )Regist_Categ_Name  from outpatient_user_info;
select count(distinct Visit_Status_Flag )Visit_Status_Flag  from outpatient_user_info;
select count(distinct Emerg_Flag        )Emerg_Flag         from outpatient_user_info;
select count(distinct TCM_Observe_Result)TCM_Observe_Result from outpatient_user_info;



outpatient_physical_report
select SEX_CODE         ,count(1) from outpatient_physical_report group by SEX_CODE         ;
select PAT_RESOURCE_CODE,count(1) from outpatient_physical_report group by PAT_RESOURCE_CODE;
select PAT_RESOURCE_NAME,count(1) from outpatient_physical_report group by PAT_RESOURCE_NAME;
select EXAM_PART_NAME   ,count(1) from outpatient_physical_report group by EXAM_PART_NAME   ;
select EXAM_DESC        ,count(1) from outpatient_physical_report group by EXAM_DESC order by  count(1) desc      ;
select EXAM_RESULT      ,count(1) from outpatient_physical_report group by EXAM_RESULT   order by  count(1) desc   ;
select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT year( from_days( datediff( now( ), Date_Birth))) age from outpatient_physical_report) tin) t 
group by age_distribute;

select count(distinct OutHosp_Index_No  ) from outpatient_physical_report;
select count(distinct SEX_CODE         ) from outpatient_physical_report;
select count(distinct DATE_BIRTH       ) from outpatient_physical_report;
select count(distinct PAT_RESOURCE_CODE) from outpatient_physical_report;
select count(distinct PAT_RESOURCE_NAME) from outpatient_physical_report;
select count(distinct EXAM_DATE        ) from outpatient_physical_report;
select count(distinct EXAM_PART_NAME   ) from outpatient_physical_report;
select count(distinct EXAM_DESC        ) from outpatient_physical_report;
select count(distinct EXAM_RESULT      ) from outpatient_physical_report;



outpatient_library_report																								   
select SEX_CODE             ,count(1) from outpatient_library_report group by SEX_CODE             ;
select DATE_BIRTH           ,count(1) from outpatient_library_report group by DATE_BIRTH           ;
select TEST_ITEM_TYPE_NAME  ,count(1) from outpatient_library_report group by TEST_ITEM_TYPE_NAME   order by count(1) desc;
select TEST_ITEM_NAME       ,count(1) from outpatient_library_report group by TEST_ITEM_NAME        order by count(1) desc;
select TEST_DETAIL_ITEM_NAME,count(1) from outpatient_library_report group by TEST_DETAIL_ITEM_NAME order by count(1) desc;
select TEST_DEPT_NAME       ,count(1) from outpatient_library_report group by TEST_DEPT_NAME        order by count(1) desc;
select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT year( from_days( datediff( now( ), Date_Birth))) age from outpatient_library_report) tin) t 
group by age_distribute;



select count(distinct OutHosp_Index_No     ) from outpatient_library_report;
select count(distinct SEX_CODE             ) from outpatient_library_report;
select count(distinct DATE_BIRTH           ) from outpatient_library_report;
select count(distinct TEST_ITEM_TYPE_CODE  ) from outpatient_library_report;
select count(distinct TEST_ITEM_TYPE_NAME  ) from outpatient_library_report;
select count(distinct TEST_ITEM_CODE       ) from outpatient_library_report;
select count(distinct TEST_ITEM_NAME       ) from outpatient_library_report;
select count(distinct TEST_DETAIL_ITEM_CODE) from outpatient_library_report;
select count(distinct TEST_DETAIL_ITEM_NAME) from outpatient_library_report;
select count(distinct TEST_DEPT_CODE       ) from outpatient_library_report;
select count(distinct TEST_DEPT_NAME       ) from outpatient_library_report;



outpatient_pres_detail
select Pres_Categ_Name         ,count(1) from outpatient_pres_detail group by Pres_Categ_Name          order by count(1) desc;
select Drug_Pres_Flag          ,count(1) from outpatient_pres_detail group by Drug_Pres_Flag           order by count(1) desc;        
select Drug_Categ_Name         ,count(1) from outpatient_pres_detail group by Drug_Categ_Name          order by count(1) desc;      
select Drug_Form_Name          ,count(1) from outpatient_pres_detail group by Drug_Form_Name           order by count(1) desc;
select Drug_Use_One_Dosage     ,count(1) from outpatient_pres_detail group by Drug_Use_One_Dosage      order by count(1) desc;
select Drug_Use_One_Dosage_Unit,count(1) from outpatient_pres_detail group by Drug_Use_One_Dosage_Unit order by count(1) desc;
select Dose_Way_Code           ,count(1) from outpatient_pres_detail group by Dose_Way_Code            order by count(1) desc;
select TCM_Use_Way             ,count(1) from outpatient_pres_detail group by TCM_Use_Way              order by count(1) desc;


       
select count(distinct Pres_No        ) from outpatient_pres_detail;
select count(distinct Pres_Categ_Name         ) from outpatient_pres_detail;
select count(distinct Drug_Pres_Flag          ) from outpatient_pres_detail;
select count(distinct Drug_Categ_Code         ) from outpatient_pres_detail;
select count(distinct Drug_Categ_Name         ) from outpatient_pres_detail;
select count(distinct Drug_Form_Code          ) from outpatient_pres_detail;
select count(distinct Drug_Form_Name          ) from outpatient_pres_detail;
select count(distinct Drug_Use_One_Dosage     ) from outpatient_pres_detail;
select count(distinct Drug_Use_One_Dosage_Unit) from outpatient_pres_detail;
select count(distinct Dose_Way_Code           ) from outpatient_pres_detail;
select count(distinct TCM_Use_Way             ) from outpatient_pres_detail;


#住院
inpatient_user_info
select Sex_Code           ,count(1) from  inpatient_user_info group by Sex_Code              order by count(1) desc;
select Occupation_Code    ,count(1) from  inpatient_user_info group by Occupation_Code       order by count(1) desc;
select Marital_Status_Code,count(1) from  inpatient_user_info group by Marital_Status_Code   order by count(1) desc;
select Admit_Dept_Name    ,count(1) from  inpatient_user_info group by Admit_Dept_Name       order by count(1) desc;
select Admit_Way_Code     ,count(1) from  inpatient_user_info group by Admit_Way_Code        order by count(1) desc;
select Admit_Method_Code  ,count(1) from  inpatient_user_info group by Admit_Method_Code     order by count(1) desc;
select Curr_Dept_Name     ,count(1) from  inpatient_user_info group by Curr_Dept_Name        order by count(1) desc;
select Discharge_Diag_Code,count(1) from  inpatient_user_info group by Discharge_Diag_Code   order by count(1) desc;
select Baby_Flag          ,count(1) from  inpatient_user_info group by Baby_Flag             order by count(1) desc;
select weight             ,count(1) from  inpatient_user_info group by weight                order by count(1) desc;
select Pat_Status         ,count(1) from  inpatient_user_info group by Pat_Status            order by count(1) desc;
select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT age from inpatient_user_info) tin) t 
group by age_distribute;


select age_distribute,count(1) from 
(select case when weight_int<=20 then '<=20' 
when weight_int>20 and weight_int<=40 then '20~40' 
when weight_int>40 and weight_int<60 then '40~60' 
when weight_int>60 then '>60' end age_distribute  
from (SELECT cast(weight as DECIMAL) weight_int from inpatient_user_info) tin) t 
group by age_distribute;




select count(distinct InHosp_Index_No    ) from inpatient_user_info;
select count(distinct Sex_Code           ) from inpatient_user_info;
select count(distinct Date_Birth         ) from inpatient_user_info;
select count(distinct age                ) from inpatient_user_info;
select count(distinct Occupation_Code    ) from inpatient_user_info;
select count(distinct Marital_Status_Code) from inpatient_user_info;
select count(distinct Admit_Dept_Code    ) from inpatient_user_info;
select count(distinct Admit_Dept_Name    ) from inpatient_user_info;
select count(distinct Admit_Way_Code     ) from inpatient_user_info;
select count(distinct Admit_Method_Code  ) from inpatient_user_info;
select count(distinct Curr_Dept_Code     ) from inpatient_user_info;
select count(distinct Curr_Dept_Name     ) from inpatient_user_info;
select count(distinct Curr_Diag_Code     ) from inpatient_user_info;
select count(distinct Discharge_Diag_Code) from inpatient_user_info;
select count(distinct Baby_Flag          ) from inpatient_user_info;
select count(distinct weight             ) from inpatient_user_info;
select count(distinct Pat_Status         ) from inpatient_user_info;


inpatient_physical_report
select SEX_CODE         ,count(1) from inpatient_physical_report group by SEX_CODE         ;
select PAT_RESOURCE_NAME         ,count(1) from inpatient_physical_report group by PAT_RESOURCE_NAME         ;
select EXAM_PART_NAME   ,count(1) from inpatient_physical_report group by EXAM_PART_NAME  order by  count(1) desc ;
select EXAM_DESC        ,count(1) from inpatient_physical_report group by EXAM_DESC order by  count(1) desc      ;
select EXAM_RESULT      ,count(1) from inpatient_physical_report group by EXAM_RESULT   order by  count(1) desc   ;
select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT year( from_days( datediff( now( ), Date_Birth))) age from inpatient_physical_report) tin) t 
group by age_distribute;

select count(distinct InHosp_Index_No  ) from inpatient_physical_report;
select count(distinct SEX_CODE         ) from inpatient_physical_report;
select count(distinct DATE_BIRTH       ) from inpatient_physical_report;
select count(distinct PAT_RESOURCE_CODE) from inpatient_physical_report;
select count(distinct PAT_RESOURCE_NAME) from inpatient_physical_report;
select count(distinct EXAM_DATE        ) from inpatient_physical_report;
select count(distinct EXAM_PART_NAME   ) from inpatient_physical_report;
select count(distinct EXAM_DESC        ) from inpatient_physical_report;
select count(distinct EXAM_RESULT      ) from inpatient_physical_report;






inpatient_library_report																								   
select SEX_CODE             ,count(1) from inpatient_library_report group by SEX_CODE             ;
select TEST_ITEM_TYPE_NAME  ,count(1) from inpatient_library_report group by TEST_ITEM_TYPE_NAME   order by count(1) desc;
select TEST_ITEM_NAME       ,count(1) from inpatient_library_report group by TEST_ITEM_NAME        order by count(1) desc;
select TEST_DETAIL_ITEM_NAME,count(1) from inpatient_library_report group by TEST_DETAIL_ITEM_NAME order by count(1) desc;
select TEST_DEPT_NAME       ,count(1) from inpatient_library_report group by TEST_DEPT_NAME        order by count(1) desc;
select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT year( from_days( datediff( now( ), Date_Birth))) age from inpatient_library_report) tin) t 
group by age_distribute;



select count(distinct InHosp_Index_No     ) from inpatient_library_report;
select count(distinct SEX_CODE             ) from inpatient_library_report;
select count(distinct DATE_BIRTH           ) from inpatient_library_report;
select count(distinct TEST_ITEM_TYPE_CODE  ) from inpatient_library_report;
select count(distinct TEST_ITEM_TYPE_NAME  ) from inpatient_library_report;
select count(distinct TEST_ITEM_CODE       ) from inpatient_library_report;
select count(distinct TEST_ITEM_NAME       ) from inpatient_library_report;
select count(distinct TEST_DETAIL_ITEM_CODE) from inpatient_library_report;
select count(distinct TEST_DETAIL_ITEM_NAME) from inpatient_library_report;
select count(distinct TEST_DEPT_CODE       ) from inpatient_library_report;
select count(distinct TEST_DEPT_NAME       ) from inpatient_library_report;



inpatient_dr_order_info
select Dept_Code               ,count(1) from inpatient_dr_order_info group by Dept_Code                order by count(1) desc;
select Sex_Code                ,count(1) from inpatient_dr_order_info group by Sex_Code                 order by count(1) desc;
select Order_Comment_Info      ,count(1) from inpatient_dr_order_info group by Order_Comment_Info       order by count(1) desc;
select Order_Cancel_Date       ,count(1) from inpatient_dr_order_info group by Order_Cancel_Date        order by count(1) desc;
select Dose_Way_Name           ,count(1) from inpatient_dr_order_info group by Dose_Way_Name            order by count(1) desc;

select age_distribute,count(1) from 
(select case when age<=6 then '<=6' 
when age>6 and age<=12 then '6~12' 
when age>12 then '>12' end age_distribute  
from (SELECT age from inpatient_dr_order_info) tin) t 
group by age_distribute;


select age_distribute,count(1) from 
(select case when weight_int<=20 then '<=20' 
when weight_int>20 and weight_int<=40 then '20~40' 
when weight_int>40 and weight_int<60 then '40~60' 
when weight_int>60 then '>60' end age_distribute  
from (SELECT cast(weight as DECIMAL) weight_int from inpatient_dr_order_info) tin) t 
group by age_distribute;

select count(distinct InHosp_Index_No     ) from inpatient_dr_order_info;
select count(distinct Dept_Code               ) from inpatient_dr_order_info;
select count(distinct Dept_Code               ) from inpatient_dr_order_info;
select count(distinct Sex_Code                ) from inpatient_dr_order_info;
select count(distinct Age                     ) from inpatient_dr_order_info;
select count(distinct Weight                  ) from inpatient_dr_order_info;
select count(distinct Order_Plan_Begin_Date   ) from inpatient_dr_order_info;
select count(distinct Order_Plan_End_Date     ) from inpatient_dr_order_info;
select count(distinct Order_Comment_Info      ) from inpatient_dr_order_info;
select count(distinct Order_Execut_Begin_Date ) from inpatient_dr_order_info;
select count(distinct Order_Execut_Finish_Date) from inpatient_dr_order_info;
select count(distinct Order_Cancel_Date       ) from inpatient_dr_order_info;
select count(distinct Dose_Way_Name           ) from inpatient_dr_order_info;





